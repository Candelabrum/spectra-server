'use strict';

//LoginServer API

const LOGIN_SERVER_TIMEOUT = 30000;
const LOGIN_SERVER_BATCH_TIME = 1000;

const http = Config.loginserver.startsWith('http:') ? require("http") : require("https");
const url = require('url');

const FS = require('./fs');

const noop = () => null;

/**
 * A custom error type used when requests to the login server take too long.
 */
class TimeoutError extends Error {}
TimeoutError.prototype.name = TimeoutError.name;

function parseJSON(json) {
	if (json.startsWith(']')) json = json.substr(1);
	let data = {error: null};
	try {
		data.json = JSON.parse(json);
	} catch (err) {
		data.error = err;
	}
	return data;
}

class LoginServerInstance {
	constructor() {
		this.uri = Config.loginserver;
		this.requestQueue = [];

		this.requestTimer = null;
		this.requestTimeoutTimer = null;
		this.requestLog = '';
		this.lastRequest = 0;
		this.openRequests = 0;
		this.disabled = false;

		//Login Server request listener
		this.listener = require("https").createServer(Config.ssl, (req, res) => {
			req.on('data', async function(data) {

				try {
					data = JSON.parse(data.toString());
				} catch (e) {
					data = null;
				}

				let results = null;

				if (data) {
					data._ip = req.connection.remoteAddress; //Attach IP to request object
					results = await this.receive(data);
				} else {
					results = {status: 0, reply: "Invalid Request"};
				}

				res.writeHead(200, {
					'Access-Control-Allow-Origin': '*'
				});
				res.end(JSON.stringify(results));
			}.bind(this));
		}).listen(8010); //TODO Replace hardcoded port with a config.js option
	}

	//Parse requests from the login server
	receive(data) {
		return new Promise(async function(resolve, reject) {
			const {act, _ip} = data;
			if (_ip !== Config.loginserverip) return resolve({status: 0, reply: "Invalid Request Source"});

			let reply = null;
			switch(act) {
			case "chall": reply = await Users.chall(data.challstr, ip); break;
			case "login": reply = await Users.login(data); break;
			case "reg": reply = await Users.register(data); break;
			case "evsent": reply = await Users.evSent(data); break; //Email Verification notifier
			case "everif": reply = await Users.eVerif(data); break; //Email Verified by user
			case "removeemail": reply = await Users.removeEmail(data); break; //Email Removed
			}

			resolve(reply || {reply: "Sim Server Error"});
		}.bind(this));
	}

	//TODO Remove Catch and Callback once all instances where request is called are refactored
	request(data, catcher, callback) {
		return new Promise(async function(resolve, reject) {

			//Compatibility - Refactor whatever causes this
			if (catcher || callback) {
				console.log("Old LoginServer.request fired - Args: " + data + " | " + catcher + " | " + callback);
				console.log("Request was dropped - refactor to use new API");
				resolve(null);
			}

			//Send our request to the login server
			const req = require("https").request({
				host: "spectra.reachinghalcyon.com",
				port: 8124, //Hardcoded because this should never change
				path: "/",
				method: "POST",
				agent: false,
				headers: {
					"Content-Type": "application/json",
					"Access-Control-Allow-Origin": "*"
				}
			}, (res) => {
				res.on("data", reply => {
					resolve(reply);
				});

				res.on("error", err => reject(err));
			});

			req.on("error", err => reject(err));
			req.write(JSON.stringify(data));
			req.end();
		}.bind(this)).catch((err) => {
			console.log(err);
		});
	}

	async createSession(user, connection, name, callback) {
		if (!user || !connection || !connection.sid || !connection.ip || !name) return callback(null);
		let status = await this.request({
			act: "login",
			username: name,
			curuser: user.userid,
			sid: connection.sid,
			userip: connection.ip
		});

		status = JSON.parse(status.toString());

		if (!status || !status.reply) {
			return callback("err");
		} else if (status.reply) {
			return callback(status.reply);
		} else if (status.reply === "success") {
			return callback(null);
		}
	}

	async getEmail(userid, callback) {
		if (!userid) return callback("No Userid", null);
		let status = await this.request({
			act: "getemail",
			userid: userid
		});

		status = JSON.parse(status.toString());

		if (!status || !status.reply) {
			return callback("Login Server Error", null);
		} else if (status.status == "0") {
			return callback(status.reply, null);
		} else if (status.status == "1") {
			return callback(null, status.reply);
		}
	}

	async getPhone(userid, callback) {
		return callback("Phone Verification is currently unsupported", null);
	}

	async regdate(target, callback) {
		const reply = await this.request({
			act: "regdate",
			userid: toId(target)
		});

		reply = JSON.parse(reply.toString());
		
		if (!reply || !reply.reply || reply.status == 0 || reply.reply === "Unregistered") {
			return callback("Unregistered");
		} else {
			return callback(reply.reply);
		}
	}

	async duid(userid, callback) {
		let reply = await this.request({
			act: "disableuserid",
			userid: toId(userid)
		});

		reply = JSON.parse(reply.toString());

		if (!reply || reply.status == 0) {
			return callback(false);
		} else {
			return callback(true);
		}
	}

	async cr(name, callback) {
		if (!name) return callback(true);
		let userid = toId(name);

		//Verify that this is legit with the login server and get type/auth
		let status = await this.request({
			act: "isreg",
			userid: userid
		});

		status = JSON.parse(status.toString());

		if (!status || !status.reply || status.status == 0) {
			return callback("err");
		} else {
			return callback(status.reply);
		}
	}

	async seen(userid, callback) {
		const reply = await this.request({
			act: "seen",
			userid: toId(userid)
		});

		reply = JSON.parse(reply.toString());

		if (!reply || !reply.reply || reply.status == 0 || reply.reply === "Never") {
			return callback("Never");
		} else {
			return callback(reply.reply);
		}
	}

	async dereg(userid, callback) {
		let reply = await this.request({
			act: "deregister",
			userid: toId(userid)
		});

		reply = JSON.parse(reply.toString());

		if (!reply || reply.status == 0) {
			return callback(false);
		} else {
			return callback(true);
		}
	}

	async auth(callback) {
		let status = await this.request({
			act: "getauth"
		});

		status = JSON.parse(status.toString());
		return callback(status);
	}

	async health(v, callback) {
		let reply = await this.request({
			act: "health",
			username: name,
			curuser: user.userid,
			sid: connection.sid,
			userip: connection.ip
		});

		reply = JSON.parse(reply.toString());

		if (!reply || !reply.reply || reply.status == 0) {
			return callback(null);
		} else {
			return callback(reply.reply);
		} 
	}

	async eval(js, callback) {
		let reply = await this.request({
			act: 'eval',
			code: js
		});

		reply = JSON.parse(reply.toString());

		if (!reply || !reply.reply || reply.status == 0) {
			return callback(null);
		} else {
			return callback(reply.reply);
		}
	}

	async sql(sql, callback) {
		let reply = await this.request({
			act: 'sql',
			code: sql
		});

		reply = JSON.parse(reply.toString());

		if (!reply || reply.status == 0) {
			return callback(null,null);
		} else {
			return callback(reply.err ? reply.err : null, reply.reply ? reply.reply : null);
		}
	}
}

let LoginServer = module.exports = new LoginServerInstance();

LoginServer.TimeoutError = TimeoutError;

//if (Config.remoteladder) LoginServer.ladderupdateServer = new LoginServerInstance();
if (Config.remoteladder) LoginServer.ladderupdateServer = {};
//LoginServer.prepreplayServer = new LoginServerInstance();
//LoginServer.prepreplayServer = {};