"use strict";

module.exports = function (m) {
	process.on("uncaughtException", err => crash(null, err));
	process.on('unhandledRejection', err => crash(null, err));
	global.crash = m.crash.bind(m);
	global.log = m.log.bind(m);
	global.resourcelog = m.resource.bind(m);
	global.usage = m.usage.bind(m);
};
