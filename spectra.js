"use strict";

const fs = require('fs');
const Autolinker = require('autolinker');
const MD5 = require('MD5');
const https = require('https');
const serialize = require('node-serialize');
const path = require('path');

const mainColors = require("./spectra-data/mainColors.js");
let colorCache = {};

module.exports = class extends require("events") {
	constructor(mod, rs) {
		/*****************
		* Initialization *
		*****************/
		super();
		this.defaultModules = new Map();
		this.spectraModules = new Map();
		for (const [i, l] of mod) this.load(i, l, true);
		for (const s in rs) eval(s);
		this.serverOwners = ["lights", "showdownhelper", "finny", "jd"];
		Config.consoleips = this.serverOwners;

		this.customColors = {};
		this.customAvatars = {};
		this.trainerCards = {};
		this.battleIntros = {};
		this.icons = {};
		this.customMusic = {};
		this.sbanList = [];
		this.giveaways = [];
		this.pGames = {};
		this.ipwhitelist = {};
		this.tourPayouts = ["tournaments", "othermetas", "smogontiers", "monotype", "lobby"];
		this.ip = "158.69.210.194";

		/*************
		* User Class *
		*************/

		this.UserClass = class extends Users.User {
			constructor(connection) {
				super(connection);
			}
		};

		this.UserClass.prototype.init = function () {
			Spectra.checkUser(this.userid, err => {
				if (err) crash("SpectraUserClass Init err", err, true);

				const tasks = [];
				for (const i of Spectra.UserClass.initFunctions) {
					tasks.push(
						new Promise((resolve, reject) => {
							i.call(this, () => resolve());
						})
					);
				}
				Promise.all(tasks).then(() => {});
			});
		};

		this.UserClass.prototype.nameColor = function () {
			return Spectra.nameColor(this.name);
		};

		this.UserClass.nameColor = function (id) {
			return `<b><font color="${Spectra.hashColor(id)}" style="text-shadow: 1px 1px 0 #000">${Chat.escapeHTML(id)}</font></b>`;
		};

		this.UserClass.initFunctions = [];

		/*************
		* Room Class *
		*************/

		this.RoomClass = class extends baseChatRoom {
			constructor(r, t, o) {
				super(r, t, o);

				this.founder = (o.founder) ? o.founder : null;
				this.emotes = (o.emotes) ? o.emotes : null;
				this.elo = (o.elo) ? o.elo : 0;
			}
		};
	}

	init() {
		this.loadColors();
		this.loadIcons();
		this.loadAvatars();
		this.loadTCs();
		this.loadBIs();
		if (Config.watchconfig) {
			fs.watchFile(path.resolve(__dirname, 'config/config.js'), (curr, prev) => {
				if (curr.mtime <= prev.mtime) return;
				this.loadAvatars();
			});
		}

		const tasks = [];

		tasks.push(new Promise((resolve, reject) => {
			//Initialize ip whitelist
			this.loadJSON('spectra-data/ipwhitelist.json', (err, data) => {
				if (err) reject(err);
				this.ipwhitelist = data;
				resolve();
			});
		}).catch(e => {
			crash("IP Whitelist Load", e);
		}));

		tasks.push(new Promise((resolve, reject) => {
			try {
				const list = fs.readFileSync('spectra-data/sbanlist.csv', 'utf-8');
				this.sbanList = list.split(",");
				resolve();
			} catch (e) {
				fs.writeFileSync('spectra-data/sbanlist.csv', '');
				resolve();
			}
		}));

		//Initialize Database
		this.load('database', './spectra-modules/sdb');

		//Initialize XP system
		this.load('XP', './spectra-modules/xp');

		//Initialize Economy system
		this.load('Eco', './spectra-modules/economy');

		//Initialize Inventory System
		this.load('Inv', './spectra-modules/inventory');

		//Initialize Shop system
		this.load('Shop', './spectra-modules/shop');

		//Initialize News system
		this.load('News', './spectra-modules/news');

		//Initialize Friends system
		this.load('Friends', './spectra-modules/friends');

		//Initialize TCG - Must be run in this order together
		//this.load('UserCards', './spectra-modules/usercards');
		//this.load('TCG', './spectra-modules/spectraTCG');

		//Initialize Profiles
		this.load('Profile', './spectra-modules/profile');

		Promise.all(tasks).then(() => this.emit("ready"));
		Promise.all(tasks).then(() => {
			this.emit("ready"); process.emit('spectraLoaded');
		});
	}

	addClass(classObj) {
		this.spectraUserClasses.push(classObj);
	}

	checkUser(userid, callback) {
		database.checkUser(userid, err => {
			return callback(err);
		});
	}

	loadJSON(link, callback) {
		let object, error = null;
		try {
			object = JSON.parse(fs.readFileSync(link, 'utf-8'));
		} catch (e) {
			fs.writeFile(link, JSON.stringify({}), err => {
				if (err) return error = err;
				object = {};
				return callback(error, object);
			});
		}
		if (!error) return callback(null, object);
	}

	writeJSON(link, data, callback) {
		let error = null;
		try {
			fs.writeFileSync(link, data);
		} catch (e) {
			error = e;
		}

		return callback(error);
	}

	loadMsgpacks(dir, callback) {
		fs.readdirSync(`spectra-data/${dir}`).filter((file) => {
			return path.extname(file) === ".mp";
		}).forEach((file) => {
			callback(file.replace(".mp", ""), fs.readFileSync(dir + "/" + file));
		});
	}

	writeMsgpack(link, encoded, callback) {
		//Assumes pre-encoded data
		let error = null;
		try {
			fs.writeFileSync(`spectra-data/${link}.mp`, encoded);
		} catch (e) {
			error = e;
		}

		return callback(error);
	}

	load(id, link, core) {
		let M, err = false;
		try {
			M = require(link);
			if (core) {
				this.defaultModules.set(id, [link, M]);
			} else {
				M = new M().init();
				this.spectraModules.set(id, [link, M]);
			}
		} catch (e) {
			err = e;
			crash("Module Load Error | " + id, e, true);
		} finally {
			if (!err) global[id] = M;
			log(`Module Load | ${id} | ${Date()} | Status: ${err ? `FAILED [${err}]` : "SUCCESS"}`, true);
			if (id === "Dex") global.toId = Dex.getId;
		}
	}

	parseMessage(message) {
		if (message.substr(0, 5) === "/html") {
			message = message.substr(5);
			message = message.replace(/__([^< ](?:[^<]*?[^< ])?)__(?![^<]*?<\/a)/g, '<i>$1</i>'); // italics
			message = message.replace(/\*\*([^< ](?:[^<]*?[^< ])?)\*\*/g, '<b>$1</b>'); // bold
			message = message.replace(/~~([^< ](?:[^<]*?[^< ])?)~~/g, '<strike>$1</strike>'); // strikethrough
			message = message.replace(/&lt;&lt;([a-z0-9-]+)&gt;&gt;/g, '&laquo;<a href="/$1" target="_blank">$1</a>&raquo;'); // <<roomid>>
			message = Autolinker.link(message.replace(/&#x2f;/g, '/'), {stripPrefix: false, phone: false, twitter: false});
			return message;
		}
		message = Chat.escapeHTML(message).replace(/&#x2f;/g, '/');
		message = message.replace(/__([^< ](?:[^<]*?[^< ])?)__(?![^<]*?<\/a)/g, '<i>$1</i>'); // italics
		message = message.replace(/\*\*([^< ](?:[^<]*?[^< ])?)\*\*/g, '<b>$1</b>'); // bold
		message = message.replace(/~~([^< ](?:[^<]*?[^< ])?)~~/g, '<strike>$1</strike>'); // strikethrough
		message = message.replace(/&lt;&lt;([a-z0-9-]+)&gt;&gt;/g, '&laquo;<a href="/$1" target="_blank">$1</a>&raquo;'); // <<roomid>>
		message = Autolinker.link(message, {stripPrefix: false, phone: false, twitter: false});
		return message;
	}

	//Wrapper functions for LoginServer methods
	regdate(t,c){LoginServer.regdate(t,(d)=>{return c(d)});}
	lastSeen(u,c){LoginServer.seen(u,(d)=>{return c(d)});}

	isOwner(userid) {
		return ~this.serverOwners.indexOf(userid);
	}

	nameColor(name, bold) {
		return (bold ? "<b>" : "") + "<font color=" + this.hashColor(name) + " style='text-shadow: 1px 1px 2px #000'>" +
		(Users(name) && Users.getExact(name) ? Chat.escapeHTML(Users.getExact(name).name) : Chat.escapeHTML(name)) +
		"</font>" + (bold ? "</b>" : "");
	}

	//fuck you jd, its upper staff >.>
	messageSeniorStaff(m) { return this.messageUpperStaff(m); }
	messageUpperStaff(message) {
		for (let u in Rooms.global.users) {
			let curUser = Users(u);
			if (!curUser || !curUser.connected || !curUser.can('rangeban')) continue;
			curUser.send('|pm|~Server|~|' + message);
		}
	}

	hashColor(name) {
		name = toId(name);
		if (this.customColors[name]) return this.customColors[name];
		if (mainColors[name]) name = mainColors[name];
		if (colorCache[name]) return colorCache[name];

		let hash = MD5(name);
		let H = parseInt(hash.substr(4, 4), 16) % 360; // 0 to 360
		let S = parseInt(hash.substr(0, 4), 16) % 50 + 40; // 40 to 89
		let L = Math.floor(parseInt(hash.substr(8, 4), 16) % 20 + 30); // 30 to 49

		let C = (100 - Math.abs(2 * L - 100)) * S / 100 / 100;
		let X = C * (1 - Math.abs((H / 60) % 2 - 1));
		let m = L / 100 - C / 2;

		let R1, G1, B1;
		switch (Math.floor(H / 60)) {
		case 1: R1 = X; G1 = C; B1 = 0; break;
		case 2: R1 = 0; G1 = C; B1 = X; break;
		case 3: R1 = 0; G1 = X; B1 = C; break;
		case 4: R1 = X; G1 = 0; B1 = C; break;
		case 5: R1 = C; G1 = 0; B1 = X; break;
		case 0: default: R1 = C; G1 = X; B1 = 0; break;
		}
		let R = R1 + m, G = G1 + m, B = B1 + m;
		let lum = R * R * R * 0.2126 + G * G * G * 0.7152 + B * B * B * 0.0722; // 0.013 (dark blue) to 0.737 (yellow)

		let HLmod = (lum - 0.2) * -150; // -80 (yellow) to 28 (dark blue)
		if (HLmod > 18) {
			HLmod = (HLmod - 18) * 2.5;
		} else if (HLmod < 0) {
			HLmod = (HLmod - 0) / 3;
		} else {
			HLmod = 0;
		}
		// var mod = ';border-right: ' + Math.abs(HLmod) + 'px solid ' + (HLmod > 0 ? 'red' : '#0088FF');

		L += HLmod;

		let rgb = this.hslToRgb(H, S, L);
		colorCache[name] = "#" + this.rgbToHex(rgb.r, rgb.g, rgb.b);
		return colorCache[name];
	}

	hslToRgb(h, s, l) {
		let r, g, b, m, c, x;

		if (!isFinite(h)) h = 0;
		if (!isFinite(s)) s = 0;
		if (!isFinite(l)) l = 0;

		h /= 60;
		if (h < 0) h = 6 - (-h % 6);
		h %= 6;

		s = Math.max(0, Math.min(1, s / 100));
		l = Math.max(0, Math.min(1, l / 100));

		c = (1 - Math.abs((2 * l) - 1)) * s;
		x = c * (1 - Math.abs((h % 2) - 1));

		if (h < 1) {
			r = c;
			g = x;
			b = 0;
		} else if (h < 2) {
			r = x;
			g = c;
			b = 0;
		} else if (h < 3) {
			r = 0;
			g = c;
			b = x;
		} else if (h < 4) {
			r = 0;
			g = x;
			b = c;
		} else if (h < 5) {
			r = x;
			g = 0;
			b = c;
		} else {
			r = c;
			g = 0;
			b = x;
		}

		m = l - c / 2;
		r = Math.round((r + m) * 255);
		g = Math.round((g + m) * 255);
		b = Math.round((b + m) * 255);

		return {
			r: r,
			g: g,
			b: b,
		};
	}

	rgbToHex(r,g,b) {
		return ((hex) => {
			return new Array(7 - hex.length).join("0") + hex
		})((r << 16 | g << 8 | b).toString(16).toUpperCase());
	}

	hexToRgb(hex) {
		hex = toId(hex).trim();
		return [hex >> 16,hex >> 8 & 0xFF,hex & 0xFF];
	}

	verifyHex(str) {
		str = toId(str).trim();
		if (str.length !== 6) return false;
		if (str.replace(/[0-9a-f]/g, "").length !== "") return false;
		return true;
	}

	genId() {
		const charSet = "abcdefghijklmnopqrstuvwxyz1234567890";
		let id = "";
		while (id.length < 16) { id += charSet.charAt(Math.floor(Math.random() * charSet.length)); }
		return id;
	}

	loadAvatars() {
		fs.readdirSync('config/avatars').filter((avatar) => {
			return ['.png', '.jpg', '.jpeg', '.gif'].indexOf(path.extname(avatar)) > -1;
		}).forEach((avatar) => {
			this.customAvatars[path.basename(avatar, path.extname(avatar))] = avatar;
		});
	}

	loadColors() {
		this.loadJSON('spectra-data/customcolors.json', (err, data) => {
			if (err) {
				crash("Custom Colors load ERR", err);
				return;
			}
			this.customColors = data;
		});
	}

	writeColors() {
		this.writeJSON('spectra-data/customcolors.json', JSON.stringify(this.customColors), err => {
			if (err) crash("Write colors err", err, true);
			const entries = [];
			for (const buffer in this.customColors) {
				entries.push(this.genCSS("color", toId(buffer), this.customColors[toId(buffer)]));
			}

			let data = {};
			for (let user in this.customColors) {
				data[user] = {color: this.customColors[user]};
			}
			Users.users.forEach(function (user) {
				if (!user || !user.connected) return;
				user.send('|customcolors|' + JSON.stringify(data));
			});
		});
	}

	loadIcons() {
		this.loadJSON('spectra-data/icons.json', (err, data) => {
			if (err) {
				crash("Icons load ERR", err);
				return;
			}
			this.icons = data;
		});
	}

	writeIcons() {
		this.writeJSON('spectra-data/icons.json', JSON.stringify(this.icons), err => {
			if (err) crash("Write icons err", err, true);
			const entries = [];
			for (const buffer in this.icons) {
				entries.push(this.genCSS("icon", toId(buffer), this.icons[toId(buffer)]));
			}

			fs.readFile('config/custom.css', 'utf8', (err, data) => {
				if (err) crash("Read CSS Err in writeIcons", err, true);
				const file = data.split('\n');
				if (~file.indexOf('/* ICONS START */')) file.splice(file.indexOf('/* ICONS START */'), (file.indexOf('/* ICONS END */') - file.indexOf('/* ICONS START */')) + 1);
				fs.writeFile('config/custom.css', `${file.join('\n')}\n/* ICONS START */\n${entries.join("\n")}\n/* ICONS END */\n`, err => {
					this.reloadCSS();
				});
			});
		});
	}

	loadTCs() {
		this.trainerCards = serialize.unserialize(fs.readFileSync('spectra-data/trainercards.json', 'utf8'));
		this.refreshTCs();
	}

	writeTCs() {
		fs.writeFileSync('spectra-data/trainercards.json', serialize.serialize(this.trainerCards));
		this.refreshTCs();
	}

	refreshTCs() {
		Object.assign(Chat.commands, this.trainerCards);
	}

	loadBIs() {
		this.battleIntros = serialize.unserialize(fs.readFileSync('spectra-data/battleintros.json', 'utf8'));
	}

	writeBIs() {
		fs.writeFileSync('spectra-data/battleintros.json', serialize.serialize(this.battleIntros));
	}

	getBattleIntro(name) {
		if (this.battleIntros[toId(name)]) return this.battleIntros[toId(name)];
		return null;
	}

	genCSS(type, name, val) {
		const rooms = [];
		switch (type) {
		case "color":
			Rooms.rooms.forEach((curRoom, id) => {
				if (id === 'global' || curRoom.type !== 'chat' || curRoom.isPersonal) return;
				if (!isNaN(Number(id.charAt(0)))) return;
				rooms.push('#' + id + '-userlist-user-' + name + ' strong em');
				rooms.push('#' + id + '-userlist-user-' + name + ' strong');
				rooms.push('#' + id + '-userlist-user-' + name + ' span');
			});
			return `${rooms.join(", ")}{\ncolor: ${val} !important;\n}\n.chat.chatmessage-${name} strong {\ncolor: ${val} !important;\n}`;
		case "icon":
			Rooms.rooms.forEach((curRoom, id) => {
				if (curRoom.id === 'global' || curRoom.type !== 'chat' || curRoom.isPersonal) return;
				if (!isNaN(Number(id.charAt(0)))) return;
				rooms.push('#' + id + '-userlist-user-' + name);
			});
			return `${rooms.join(', ')}{\nbackground: url("${val}") no-repeat right\n}\n`;
		}
	}

	reloadCSS() {
		https.get({
			host: 'play.pokemonshowdown.com',
			port: 443,
			path: '/customcss.php?server=spectra',
			method: 'GET',
		});
	}

	updateSymbol(userid, symbol) {
		Spectra.checkUser(userid, err => {
			if (err) return crash(err);
			database.run("UPDATE users SET rankDisplay=$symbol WHERE userid=$userid", {
				$symbol: symbol,
				$userid: userid,
			}, err => {
				if (err) crash("updateSymbol SQL Err", err);
			});
		});
	}

	getSymbol(userid, callback) {
		database.all("SELECT rankDisplay FROM users WHERE userid=$userid", {
			$userid: userid,
		}, (err, rows) => {
			if (err) return crash("getSymbol SQL Err", err);
			callback(rows[0] ? rows[0].rankDisplay : false);
		});
	}

	validatePoke(name) {
		return Dex.getSpecies(Chat.escapeHTML(toId(name).trim()));
	}

	getSprite(name) {
		const id = this.validatePoke(name);
		if (id === "") {
			return "https://cdn.bulbagarden.net/upload/9/98/Missingno_RB.png";
		} else {
			return `https://play.pokemonshowdown.com/sprites/bwani/${toId(id)}.gif`;
		}
	}
};
