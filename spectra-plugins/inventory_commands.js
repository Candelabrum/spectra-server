"use strict";

exports.commands = {
	use: function (target, room, user) {
		if (!Inv.enabled) return false;
		if (!target) return this.sendReply("/use [itemID], [...args,]* - Some items may require an additional piece of information to be used, for example an Avatartoken will require an image to be submitted.");
		let parts = target.split(',');
		for (const i in parts) {
			parts[i] = parts[i].trim();
		}
		const id = Chat.escapeHTML(parts.shift());

		//Validate action
		if (!Inv.itemLib[id]) return this.sendReply("Invalid item ID.");
		if (!user.inventory[id]) return this.sendReply("You do not own this item.");
		if (!Inv.sortList(user, 'usable', true).has(id)) return this.sendReply("You cannot use this item.");

		//Run use function
		Inv.itemLib[id].use.call(this, user, ...parts);
	},

	inventory: function (target, room, user) {
		if (!Inv.enabled) return false;
		if (!this.runBroadcast()) return;
		if (!target) target = user.userid;
		target = Chat.escapeHTML(target);
		const inventory = !Users(target) ? Inv.data[target] : Users(target).inventory;
		if (!inventory) {
			Inv.data[target] = {};
			if (Users(target)) Users(target).inventory = {};
			Inv.update();
		}
		if (Object.keys(inventory).length === 0) return this.sendReply("This user has no items in their inventory.");

		let innerHTML = [];
		let item, counter = 0;
		for (const i in inventory) {
			if (!Inv.itemLib[i]) continue; //Null item
			item = Inv.itemLib[i];

			innerHTML.push(`<td><button style="border: 1px #BA85FF solid; border-radius: 3px; box-shadow: 1px 1px 5px;" name="send" value="/viewitem ${i}"><table border=0><td style="padding: 1px"><center><img src="${item.img}" height=24 width=24></center></td><tr><td><center><b>${item.name}</b></center></td><tr><td><center>${inventory[i]}x</center></td></table></button></td>`);

			if (counter % 8 === 0 && counter !== 0) innerHTML.push("</table><br><table border=0>");

			counter++;
		}

		const outputHTML = `<div class="infobox-limited"><center><font size=3><b>Inventory of ${Users(target) ? Users(target).name : target}</b></font></center><br><center><table border=0>` + innerHTML.join("") + "</table></div>";
		this.sendReplyBox(outputHTML);
		if (this.broadcasting) room.update();
	},

	viewitem: function (target, room, user) {
		if (!this.runBroadcast()) return;
		if (!target) return this.errorReply("You must specify an item to view.");
		target = Chat.escapeHTML(toId(target).trim());
		if (!Inv.itemLib[target]) return this.errorReply("Invalid item ID.");
		const item = Inv.itemLib[target];

		//Generate property array
		const properties = [];

		if (item.use) properties.push("Usable");
		if (item.consumable) properties.push("Consumable");
		if (item.tradeable) properties.push("Tradeable");
		if (item.key) properties.push("Key");

		let html = `<img src="${item.img}" height=24 width=24><br><b>Name</b>: ${item.name}<br><b>Description</b>: ${item.desc}<br><b>Properties</b>: ${properties.join(", ")}${item.by ? `<br>Commissioned by: ${item.by}` : ""}`;
		this.sendReplyBox(html);
		if (this.broadcasting) room.update();
	},

	rejectitem: function (target, room, user) {
		if (!this.can("rangeban")) return false;
		if (!target) return this.errorReply("You must specify a user.");
		target = target.split(",");
		if (target.length !== 2) return this.sendReply("/rejectitem [userid], [item]");
		for (const i in target) target[i] = Chat.escapeHTML(toId(target[i]).trim());
		if (target[0].length >= 19) return this.sendReply("Usernames are not this long.");
		const valid = ["avatar", "icon", "room", "fix", "music", "color", "infobox", "battleintro"];

		if (valid.indexOf(target[1]) === -1) return this.sendReply(`Invalid item. Valid Items: ${valid.join(", ")}`);
		Spectra.messageUpperStaff(`/raw <center>${user.name} has declined ${Users(target[0]) ? Users(target[0]).name : target[0]}'s redeemed ${target[1].charAt(0).toUpperCase() + target[1].slice(1)}<br><br><hr></center>`);
		if (Users(target[0])) Users(target[0]).send(`|pm|~Server|~|Your ${target[1].charAt(0).toUpperCase() + target[1].slice(1)} was declined by ${user.name}.`);
	},

	giveitem: function (target, room, user) {
		if (!user.can('rangeban')) return false;
		if (!target) return this.errorReply("/giveitem [user], [itemid], [quantity]");
		let parts = target.split(",");
		for (const i in parts) parts[i] = Chat.escapeHTML(toId(parts[i]).trim());
		parts[2] = parseInt(parts[2]);
		if (isNaN(parts[2])) return this.errorReply("Quantity must be an integer.");
		if (parts.length !== 3) return this.errorReply("/giveitem [user], [itemid], [quantity]");
		if (!Inv.itemLib[parts[1]]) return this.errorReply("Invalid item ID.");
		Users(parts[0], false, true).addItem(parts[1], parts[2], parts[0]);
		if (Users(parts[0])) Users(parts[0]).popup(`${user.name} has given you ${parts[2]} ${Inv.itemLib[parts[1]].name}${parts[2] > 1 ? "s" : ""}.`);
		this.logModCommand(`${user.name} gave ${parts[2]} ${Inv.itemLib[parts[1]].name}${parts[2] > 1 ? "s" : ""} to ${Users(parts[0]) ? Users(parts[0]).name : parts[0]}.`);
		user.popup(`You have given ${parts[2]} ${Inv.itemLib[parts[1]].name}${parts[2] > 1 ? "s" : ""} to ${Users(parts[0]) ? Users(parts[0]).name : parts[0]}.`);
	},

	takeitem: function (target, room, user) {
		if (!user.can('rangeban')) return false;
		if (!target) return this.errorReply("/takeitem [user], [itemid], [quantity]");
		let parts = target.split(",");
		for (const i in parts) parts[i] = Chat.escapeHTML(toId(parts[i]).trim());
		parts[2] = parseInt(parts[2]);
		if (isNaN(parts[2])) return this.errorReply("Quantity must be an integer.");
		if (parts.length !== 3) return this.errorReply("/takeitem [user], [itemid], [quantity]");
		if (!Inv.itemLib[parts[1]]) return this.errorReply("Invalid item ID.");
		Users(parts[0], false, true).removeItem(parts[1], parts[2], parts[0]);
		if (Users(parts[0])) Users(parts[0]).popup(`${user.name} has taken ${parts[2]} ${Inv.itemLib[parts[1]].name}${parts[2] > 1 ? "s" : ""} from you.`);
		this.logModCommand(`${user.name} took ${parts[2]} ${Inv.itemLib[parts[1]].name}${parts[2] > 1 ? "s" : ""} from ${Users(parts[0]) ? Users(parts[0]).name : parts[0]}.`);
		user.popup(`You have taken ${parts[2]} ${Inv.itemLib[parts[1]].name}${parts[2] > 1 ? "s" : ""} from ${Users(parts[0]) ? Users(parts[0]).name : parts[0]}.`);
	},
};
