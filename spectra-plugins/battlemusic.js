'use strict';

const fs = require('fs');
const URL = require('url');
const ytdl = require('ytdl-core');

function saveMusic() {
	fs.writeFileSync('spectra-data/battlemusic.json', JSON.stringify(Spectra.battleMusic));
}

process.on('spectraLoaded', function () {
	Spectra.battleMusic = {};
	try {
		Spectra.battleMusic = JSON.parse(fs.readFileSync('spectra-data/battlemusic.json', 'utf8'));
	} catch (e) {}
});

exports.commands = {
	battlemusic: {
		add: function (target, room, user) {
			if (!this.can('music')) return false;
			if (!target) return this.sendReply("Usage: /battlemusic add [user], [url], [start], [end]");

			let targetSplit = target.split(',');
			for (let u in targetSplit) targetSplit[u] = targetSplit[u].trim();

			if (!targetSplit[1]) return this.sendReply("Usage: /battlemusic add [user], [url], [start], [end]");

			let targetUser = Users(targetSplit[0]);
			let url = URL.parse(targetSplit[1]);
			let start = 0;
			let end = 0;
			let title;

			if (targetSplit[2]) start = Number(targetSplit[2]);
			if (targetSplit[3]) end = Number(targetSplit[3]);

			if (!targetUser) return this.errorReply(`User "${targetSplit[0]}" not found.`);
			if (Spectra.battleMusic[targetUser.userid]) return this.errorReply(`${targetUser.name} already has battle music.`);
			if (!url.host.includes('youtube') && !url.host.includes('youtu.be')) return this.errorReply("/battlemusic add - [url] must be a youtube link.");
			if (isNaN(start)) return this.errorReply("/battlemusic add - [start] must be a valid number.");
			if (isNaN(end)) return this.errorReply("/battlemusic add - [end] must be a valid number.");

			let filename = targetUser.userid + '.m4a';
			let starttime;
			let video = ytdl(url.href, {quality: '140'});
			let output = "";

			this.sendReply(`|uhtml|custombattlemusic|<div class="infobox">Downloading custom battle music...</div>`);

			video.pipe(fs.createWriteStream('./static/battlemusic/' + filename));

			video.once('response', () => {
				starttime = Date.now();
			});

			video.on('info', function (info, format) {
				title = info.title;
				if (!end) end = info.length_seconds * 1000;
			});

			video.on('progress', (chunkLength, downloaded, total) => {
				const floatDownloaded = downloaded / total;
				const downloadedMinutes = (Date.now() - starttime) / 1000 / 60;
				output = `Downloading custom battle music...<br />`;
				output += `${(floatDownloaded * 100).toFixed(2)}% downloaded<br />`;
				output += `(${(downloaded / 1024 / 1024).toFixed(2)}MB of ${(total / 1024 / 1024).toFixed(2)}MB)<br />`;
				output += `running for: ${downloadedMinutes.toFixed(2)}minutes<br />`;
				output += `estimated time left: ${(downloadedMinutes / floatDownloaded - downloadedMinutes).toFixed(2)}minutes`;
				this.sendReply(`|uhtmlchange|custombattlemusic|<div class="infobox">${output}</div>`);
			});

			video.on('end', () => {
				Spectra.battleMusic[targetUser.userid] = {url: 'http://reachinghalcyon.com:8000/battlemusic/' + filename, title: title, start: start, end: end};
				saveMusic();

				Users.users.forEach(function (curUser) {
					if (!curUser.connected || !curUser.spectraPlus) return;
					curUser.send(`|battlemusic|${JSON.stringify(Spectra.battleMusic)}`);
				});
				output += `<br />Custom battle music has been downloaded for ${Spectra.nameColor(targetUser.name, true)}`;
				this.sendReply(`|uhtmlchange|custombattlemusic|<div class="infobox">${output}</div>`);
				Rooms('upperstaff').add(`|raw|${Spectra.nameColor(user.name, true)} has added battle music for ${Spectra.nameColor(targetUser.name, true)}: ${Chat.escapeHTML(targetSplit[1])}`);
				Spectra.messageSeniorStaff(`/html ${Spectra.nameColor(user.name, true)} has added battle music for ${Spectra.nameColor(targetUser.name, true)}: ${Chat.escapeHTML(targetSplit[1])}`);
			});
		},
		del: 'remove',
		delete: 'remove',
		rem: 'remove',
		remove: function (target, room, user) {
			if (!this.can('music')) return false;
			if (!target) return this.sendReply("Usage: /battlemusic remove [user]");
			if (!Spectra.battleMusic[toId(target)]) return this.errorReply(`${target} does not have battle music.`);

			fs.unlinkSync(`static/battlemusic/${toId(target)}.m4a`);
			delete Spectra.battleMusic[toId(target)];
			saveMusic();

			Users.users.forEach(function (curUser) {
				if (!curUser.connected || !curUser.spectraPlus) return;
				curUser.send(`|battlemusic|${JSON.stringify(Spectra.battleMusic)}`);
			});

			this.sendReply(`You've removed battle music from ${target}`);
			Rooms('upperstaff').add(`|raw|${Spectra.nameColor(user.name, true)} has removed battle music from ${Spectra.nameColor(target, true)}`);
			Spectra.messageSeniorStaff(`/html ${Spectra.nameColor(user.name, true)} has removed battle music from ${Spectra.nameColor(target, true)}`);
		},
		list: function (target, room, user) {
			if (!this.runBroadcast()) return;
			if (Object.keys(Spectra.battleMusic).length < 1) return this.sendReplyBox("No one has custom battle music.");

			let output = "Users with custom battle music:";
			for (let u in Spectra.battleMusic) {
				output += `<br />${Spectra.nameColor(u, true)}: ${Chat.escapeHTML(Spectra.battleMusic[u].title)} (${Chat.escapeHTML(Spectra.battleMusic[u].url)})`;
			}

			return this.sendReplyBox(output);
		},
		'': 'help',
		help: function (target, room, user) {
			if (!this.runBroadcast()) return;
			this.sendReplyBox(
				"Custom Battle Music Commands:<br />" +
				"/battlemusic add [user], [youtube link], [start (optional)], [end (optional)] - Gives a user custom battle music. [start] and [end] may be specified to loop between a portion of the song, rather than the entire song.<br />" +
				"/battlemusic remove [user] - Removes custom battle music from a user.<br />" +
				"/battlemusic list - Lists all users with custom music added."
			);
		},
	},
};
