'use strict';

const moment = require("moment");

const loginserver = {
	regdate: function(target, room, user) {
		if (toId(target).length < 1 || toId(target).length > 19) return this.sendReply("Usernames may not be less than one character or longer than 19");
		if (!this.runBroadcast()) return;
		LoginServer.regdate(toId(target), date => {
			this.sendReplyBox(Spectra.nameColor(target, true) + (date ? " was registered on " + moment(date).format("dddd, MMMM DD, YYYY HH:mmA ZZ") : " is not registered."));
			room.update();
		});
	},

	disableuserid: function(target, room, user) {
		if (!user.isSysop) return false;
		if (!target) return this.errorReply("Usage: /disableuserid [username] - Prevents a username from being logged into.");
		let userid = toId(target);
		if (userid.length > 18) return this.errorReply("Usernames cannot be over 18 characters.");
		if (userid.length < 1) return this.errorReply("/disableuserid - Please specify a name.");

		LoginServer.duid(userid, success => {
			if (success) {
				if (Users(userid)) {
					Ladders.matchmaker.cancelSearch(Users(userid));
					Users(userid).resetName(true);
					Users(userid).send("|nametaken||Your userid has been disabled and is not able to be used. Please select a new name.");
				}
				this.sendReply("You have deregistered the userid " + userid);
			} else {
				this.sendReply("Unable to disable Userid - Login Server Error");
			}
		});
	},

	deregister: function(target, room, user) {
		if (!user.isSysop) return false;
		if (!target) return this.errorReply("Usage: /deregister [username] - Deregisters a userid.");
		let userid = toId(target);
		if (userid.length > 18) return this.errorReply("Usernames cannot be over 18 characters.");
		if (userid.length < 1) return this.errorReply("/deregister - Please specify a name.");
		LoginServer.dereg(userid, success => {
			if (success) {
				if (Users(userid)) {
					Users(userid).registered = false;
					Users(userid).autoconfirmed = false;
					Users(userid).group = " ";
					Users(userid).updateIdentity();
					for (const connection of Users(userid).connections) {
						connection.send(`|identity|${user.name}|${Users(userid).registered ? "1" : "0"}|${Users(userid).verified ? "1" : "0"}|${connection.sid}|${Users(userid).avatar}`);
					}
				}
				this.sendReply("You have deregistered the userid " + userid);
			} else {
				this.sendReply("Unable to deregister Userid - Login Server Error");
			}
		});
	},

	seen: function(target, room, user) {
		if (!target) return this.errorReply("Usage: /seen [username] - Show's the last time the user was online.");
		if (!this.runBroadcast()) return;
		let userid = toId(target);
		if (userid.length > 18) return this.errorReply("Usernames cannot be over 18 characters.");
		if (userid.length < 1) return this.errorReply("/seen - Please specify a name.");
		let userName = '<strong class="username">' + Spectra.nameColor(target, false) + '</strong>';
		if (userid === user.userid) return this.sendReplyBox(userName + ", have you looked in a mirror lately?");
		if (Users(target) && Users(target).connected) return this.sendReplyBox(userName + ' is currently <font color="green">online</font>.');
		LoginServer.seen(userid, seen => {
			if (!seen || seen === "Never") return this.sendReplyBox(userName + ' has <font color="red">never</font> logged into Spectra.');
			this.sendReplyBox(userName + ' was last seen online on ' + moment(seen).format("MMMM Do YYYY, h:mm:ss A") + ' EST. (' + moment(seen).fromNow() + ')');
			room.update();
		});
	},

	auth: function (target, room, user, connection) {
		return connection.popup("Server Stafflist is unavailable. Please try again later.");
		LoginServer.auth(status => {
			if (!status || !status.reply || status.status == 0) {
				connection.popup("Server Stafflist is unavailable. Please try again later.");
			} else {

				const data = status.reply.split("|");
				const ranks = new Map([
					["+", new Set()],
					["%", new Set()],
					["@", new Set()],
					["&", new Set()],
					["~", new Set()]
				]);
				const titles = new Map([
					["+", "Voices"],
					["%", "Drivers (Trial Staff)"],
					["@", "Moderators"],
					["&", "Managers"],
					["~", "Administrators"]
				]);

				let parts, s;
				for (const i of data) {
					parts = data.split(",");
					s = ranks.get(parts[1]);
					s.add(parts[0]);
					ranks.set(parts[1], s);
				}

				let buffer = [];
				let arr;
				for (const [symb, rset] of ranks) {
					if (rset.size === 0) continue;
					arr = [];
					for (const entry of rset) {
						if (Users(toId(entry)) && Users(toId(entry)).connected) {
							arr.push(`<b>${entry}</b>`);
						} else {
							arr.push(`${entry}`);
						}
					}
					buffer.push(`<b>${tiles.get(symb)}</b> (Online: ${rset.size}):<br>${arr.join(", ")}`);
				}

				if (buffer.length === 0) {
					connection.popup("Server Stafflist is unavailable. Please try again later.");
				} else {
					connection.send(`|popup||wide||html|${buffer.join("<br><br>")}`);
				}
			}
		});
	},

	health: function(target, room, user) {
		if (!user.isSysop) return false;
		if (!this.runBroadcast()) return;
		const v = target && (toId(target) === "v" || toId(target) === "verbose") ? true : false;
		LoginServer.health(v, reply => {
			if (!reply) return this.sendReply("Unable to fetch LoginServer health.");
			this.sendReply(reply);
			room.update();
		});
	},

	eval: function(target, room, user) {
		if (!user.isSysop) return false;
		if (!this.runBroadcast()) return;
		const js = target ? target : "";
		LoginServer.eval(js, reply => {
			if (!reply) return this.sendReply("Unable to fetch LoginServer eval reply.");
			this.sendReply(`>> ${js}`)
			this.sendReply(`<< ${reply}`);
			room.update();
		});
	},

	sql: function(target, room, user) {
		if (!user.isSysop) return false;
		if (!this.runBroadcast()) return;
		const sql = target ? target : "";
		LoginServer.sql(sql, (err,reply) => {
			if (!err && !reply) return this.sendReply("Action Completed with no Error or Reply");
			if (err) this.sendReply(`SQL Err:\n${err}`);
			if (reply) this.sendReply(`SQL Reply:\n${reply}`);
			room.update();
		});
	}
}

exports.commands = {
	loginserver: loginserver,
	ls: loginserver,
	seen: loginserver.seen,
	regdate: loginserver.regdate,
	stafflist: loginserver.auth,
	auth: loginserver.auth,
	authlist: loginserver.auth,
	dereg: loginserver.deregister,
	deregister: loginserver.deregister,
	disableuserid: loginserver.disableuserid,
	disableuser: loginserver.disableuser,
	du: loginserver.disableuser,
	lsql: loginserver.sql,
	leval: loginserver.eval,
	lhealth: loginserver.health
};