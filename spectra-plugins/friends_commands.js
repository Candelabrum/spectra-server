"use strict";

exports.commands = {
	friends: {
		"": "list",
		view: "list",
		list: function(target, room, user) {
			if (!this.runBroadcast()) return;
			if (!target) target = user.name;
			Users(target,false,true).getFriends((friends) => {
				if (!friends || Object.keys(friends).length === 0) {
					this.sendReplyBox(Spectra.nameColor(target, true) + " has no friends.");
					return room.update();
				} else {
					//Display Friends
					let html = `<div style="max-height: 300; overflow-y: auto; overflow-x: hidden;"><b>Friends list of ${Spectra.nameColor(target,true)} (${friends.length}):</b><br>`;
					html += '<table border="1" cellspacing="0" cellpadding="3"><tr><td><u>Name</u></td><td><u>Last Seen</u></td></tr>';

					let str1, str2;
					for (const entry in friends) {
						str1 = Spectra.nameColor(entry.name, true);
						str2 = entry.seen === "Online" ? `<font color="green"><b>${entry.seen}</b></font>` : `<font color="red"><b>${entry.seen}</b></font>`;
						html += `<tr><td>${str1}</td><td>${str2}</td></tr>`;
					}

					html += "</table>";

					this.sendReplyBox(html);
					return room.update();
				}
			}, toId(target));
		},

		add: function(target, room, user) {
			if (!target) return this.errorReply("You must specify a user.");
			const friend = toId(target);
			if (friend.length < 1) return this.errorReply("Please specify a valid username.");
			if (friend.length > 19) return this.errorReply("Usernames may not be longer than 19 characters.");
			if (friend === user.userid) return this.errorReply("This server doesn\'t support your narcissism.");
			if (user.friends.size >= Friends.MAX_FRIENDS) return this.errorReply("You already have the maximum amount of friends.");

			let fail = false;
			if (user.friends.size >= 1) {
				for (const name of user.friends) {
					if (toId(name) === friend) {
						fail = true;
						break;
					}
				}
			}

			if (fail) return this.errorReply("This user is already your friend.");

			for (const name in Friends.pending) {
				if (toId(name) === user.userid) {
					for (const adder in Friends.pending[name]) {
						if (toId(adder) === friend) {
							user.friends.add(adder);
							if (Users(toId(adder)) && Users(toId(adder)).friends) {
								Users(toId(adder)).friends.add(user.name);
								Users(toId(adder)).send(`|popup|${user.name} has accepted your Friend Request.`);
							} 
							Friends.add(user.name, adder);
							delete Friends.pending[user.userid][adder];
							return this.sendReply(`You have accepted the Friend Request from ${adder}.`);
						}
					}
				}
				if (name === friend) {
					for (const adder in Friends.pending[name]) {
						if (toId(adder) === user.userid) {
							fail = "You have already sent this user a friend request.";
							break;
						}
					}
					if (Object.keys(Friends.pending[name]).length >= 5) {
						fail = "This user cannot accept any more friend requests at the moment.";
						break;
					}
				}
			}

			if (fail) return this.errorReply(fail);

			if (Friends.pending[friend]) {
				Friends.pending[friend][user.name] = true;
			} else {
				Friends.pending[friend] = {};
				Friends.pending[friend][user.name] = true;
			}

			return this.sendReply(`Friend request sent to ${friend}`);
		},

		accept: function(target, room, user) {
			if (!target) return this.errorReply("You must specify a user.");
			const friend = toId(target);
			if (friend.length < 1) return this.errorReply("Please specify a valid username.");
			if (friend.length > 19) return this.errorReply("Usernames may not be longer than 19 characters.");
			if (friend === user.userid) return this.errorReply("This server doesn\'t support your narcissism.");
			if (user.friends.size >= Friends.MAX_FRIENDS) return this.errorReply("You already have the maximum amount of friends.")
			
			let fail = false;
			if (user.friends.size >= 1) {
				for (const name of user.friends) {
					if (toId(name) === friend) {
						fail = true;
						break;
					}
				}
			}

			if (fail) return this.errorReply("This user is already your friend.");

			for (const name in Friends.pending) {
				if (name === user.userid) {
					for (const adder in Friends.pending[name]) {
						if (toId(adder) === friend) {
							user.friends.add(adder);
							if (Users(toId(adder)) && Users(toId(adder)).friends) {
								Users(toId(adder)).friends.add(user.name);
								Users(toId(adder)).send(`|popup|${user.name} has accepted your Friend Request.`);
							}
							Friends.add(user.name, adder);
							delete Friends.pending[user.userid][adder];
							return this.sendReply(`You have accepted the Friend Request from ${adder}.`);
						}
					}
				}
			}
		},

		reject: "remove",
		remove: function(target, room, user) {
			if (!target) return this.errorReply("You must specify a user.");
			target = target.trim();
			const friend = toId(target);
			if (friend.length < 1) return this.errorReply("Please specify a valid username.");
			if (friend.length > 19) return this.errorReply("Usernames may not be longer than 19 characters.");
			if (friend === user.userid) return this.errorReply("This server doesn\'t support your narcissism.");

			let del = false;
			if (user.friends.size >= 1) {
				for (const name of user.friends) {
					if (toId(name) === friend) del = name;
				}
			}

			if (del) {
				user.friends.delete(del);
				if (Users(toId(del)) && Users(toId(del)).friends) {
					Users(toId(del)).friends.delete(user.name);
				}
				Friends.delete(user.name, del);
				return this.sendReply(`You have successfully removed ${target} from your Friends List.`);
			} else if (Friends.pending[user.userid]) {
				for (const index in Friends.pending[user.userid]) {
					if (friend === toId(Friends.pending[user.userid][index])) {
						del = Friends.pending[user.userid][index];
						break;
					}
				}

				if (del) {
					delete Friends.pending[user.userid][del];
					if (Users(toId(del))) Users(toId(del)).send(`|popup|${user.name} has rejected your Friend Request.`);
					return this.sendReply(`You have rejected the Friend request from ${target}.`);
				} else {
					return this.sendReply("This user is not on your Friends List, nor do you have a pending Friend request from them.");
				}
			}
		},

		clear: "removeall",
		removeall: function(target, room, user) {
			if (!user.confirmClearFriends) {
				user.confirmClearFriends = true;
				return this.errorReply("This will remove all friends from your Friends list. If you are sure you want to do this, enter the command again.");
			}
			delete user.confirmClearFriends;
			database.run("DELETE FROM friends WHERE userid=$userid OR friend=$name", {
				$userid: user.userid,
				$name: user.name
			}, err => {
				if (err) crash("Friends Clear err", err)
				return this.sendReply("Your Friends list has been cleared.");
			});
		},

		notify: function(target, room, user) {
			return this.sendReply("Coming Soon.");
		},

		help: function(target, room, user) {
			return this.sendReply("Coming Soon.");
		},
	}
};