"use strict";

const fs = require('fs');
const scripts = require("./buyScripts.js");

module.exports = class {
	constructor() {
		this.data = null;
		this.closed = true;
		this.headerHTML = '<div class="broadcast-red"><center><font size=4><i><b>Spectra Server Shop</b></i></font></center></div><br/><br/>';
	}

	init() {
		Spectra.loadJSON('spectra-data/shop.json', (err, data) => {
			if (err) {
				crash("ShopData Load Error", err);
				this.closed = true;
			} else {
				this.buildShop(data);
				this.buildDisplays();

				Spectra.UserClass.initFunctions.push(function (callback) {
					this.shopCounter = 0;
					this.shop = null;
				});
				this.closed = false;
			}
		});
		return this;
	}

	spawnInstance(user, room) {
		return new ShopInstance(user, room);
	}

	buildShop(data) {
		//Initialize Main Shop Container
		let obj = new Menu(false, true); //Future this.data

		//Iterate through data to build the root menu
		for (const i in data) {
			if (data[i].price) {
				//Root menu only contains items
				obj.type = "items";
				obj.children.set(i, this.buildItem(i, data[i]));
			} else {
				//Root menu contains submenus
				obj.type = "container";
				obj.children.set(i, this.buildMenu(i, data[i]));
			}
		}

		this.data = obj;
	}

	buildMenu(id, data) { //Recursively build structure
		let obj = new Menu(id, false);
		for (const i in data) {
			switch (i) {
			case "display": obj.display = data[i]; break;
			case "info": obj.info = data[i]; break;
			case "children": //Build Child Menu
				obj.type = "container";
				for (const x in data.children) {
					obj.children.set(x, this.buildMenu(x, data.children[x]));
				}
				break;
			case "items": //Build Items in menu
				obj.type = "items";
				for (const x in data.items) {
					obj.children.set(x, this.buildItem(x, data.items[x]));
				}
				break;
			}
		}
		return obj;
	}

	buildItem(id, data) {
		return new Item(id, data);
	}

	buildDisplays() {
		//Draw Child Menus recursively
		const loop = function (children, parent, callback) {
			for (const [id, obj] of children) {
				if (obj.type === "items") {
					children.get(id).html = this.itemMenu(obj);
					loop(children.get(id).children, obj, (key, html) => {
						children.get(id)[key] = html;
					});
				} else if (obj.type === "container") {
					children.get(id).html = this.containerMenu(obj);
					loop(children.get(id).children);
				} else {
					if (callback) return callback(id, this.itemMenuView(obj, parent));
				}
			}
		}.bind(this);

		//Draw root menu html
		switch (this.data.type) {
		case "container":
			this.data.html = this.containerMenu(this.data);
			loop(this.data.children);
			break;
		case "items":
			this.data.html = this.itemMenu(this.data);
			loop(this.data.children, this.data, (key, html) => {
				this.data[key] = html;
			});
			break;
		}
	}

	containerMenu(menu) {
	    const childHTML = [];
	    for (const [id, obj] of menu.children) {
			childHTML.push(`<td style="padding: 8px;"><button class="shopbutton" name="send" value="/shop ${id}">${obj.display}</button></td>`);
			if (childHTML.length % 3 === 0) childHTML[-1] += '</table><br/><table border=0>';
	    }
	    return `<div><center><table border=0>${childHTML.join("")}</table></center><br/></div>`;
	}

	itemMenu(menu) {
		const childHTML = [];
		for (const [id, obj] of menu.children) {
			childHTML.push(`<td style="padding: 5px;"><button class="shopbutton" name="send" value="/shop ${id}">${obj.display}</button></td><tr>`);
		}
		return `<div style="position: relative;"><div class="leftpanel"><center><table border=0>${childHTML.join("")}</table></center><br/></div><div class="rightpanel"><br/><br/><center><b>Select an Item for more info.</b></center></div></div>`;
	}

	itemMenuView(item, menu) {
		const childHTML = [];
		for (const [id, obj] of menu.children) {
			childHTML.push(`<td style="padding: 5px;"><button class="shopbutton" name="send" value="/shop ${id}">${obj.display}</button></td><tr>`);
		}
		return `<div style="position: relative;"><div class="leftpanel"><center><table border=0>${childHTML.join("")}</table></center><br/></div><div class="rightpanel"><br><center><!-- split --><b>${item.display}</b><br><br>${item.img ? `<img src="${item.img}"><br>` : ""}<br><b>Price: ${item.price} ${item.currency}<br><br><button class="shopbutton" name="send" value="/redeem ${item.id}">${item.display}</button></center></b><!-- split --></center></div></div>`;
	}

	runTransaction(user, item, room) {
		user.getCur({userid: user.userid, type: item.currency, callback: function (val) {
			if (val >= item.price) {
				user.addCur({userid: user.userid, type: item.currency, val: -val});
				Eco.logTransaction(`${user.name} has purchased a/an ${item.display} for ${val} ${val === 1 ? `${Eco.currencies.get(item.currency)[0]}` : `${Eco.currencies.get(item.currency)[1]}`}.`);
				
				const id = Spectra.genId();
				const remaining = val - item.price;
				const receipt = `${user.userid}|${new Date().toUTCString()}|${item.display}|${item.price}|${item.currency}|${id}|${remaining}|0`;
				this.logReceipt(receipt);
				user.postPurchase(item, receipt);
				this.runScript(item.onbuy, user, room);
			} else {
				this.failedTransaction(user, item, val, room);
			}
		}.bind(this)});
	}

	failedTransaction(user, item, val, room) {
		if (!user.shop || !user.shop.cur) return null;
		const html = user.shop.cur.html;
		const parts = html.split('<!-- split -->');
		//const remaining = item.price - val;
		const output = parts[0] + '<br/><br/><center><b>You do not have enough ' + item.currency + '<br/>' + (+item.price - +val) + ' more ' + item.currency + ' requied.</b></center>' + parts[2];
		user.connections[0].sendTo(room.id, `|uhtmlchange|${user.userid}shop${user.shopCounter}|${output}`);
	}

	logReceipt(receipt) {
		fs.appendFile('logs/transactions.log', receipt + '\n');
	}

	runScript(id, user, room) {
		if (this.scripts && this.scripts[id]) this.scripts[id].call(this, user, room);
	}
};

class Menu {
	constructor(id, root) {
		this.id = id ? id : "root";
		this.root = root;
		this.children = new Map();
		this.display = this.root ? "Spectra Shop" : "";
		this.info = this.root ? "Welcome to the Spectra Shop! Currently under construction..." : "";
	}
}

class Item {
	constructor(id, data) {
		this.id = id;
		this.display = data.display || "";
		this.type = "item";
		this.price = data.price || 999999999;
		this.currency = data.currency || "points";
		this.onbuy = scripts[data.onbuy] || null;
		this.img = data.img || null;
		this.info = data.info || "";
		this.multibuy = data.multibuy || false;
	}
}

class ShopInstance {
	constructor(user, room) {
		this.user = user;
		this.room = room;
		this.root = Shop.data;
		this.last = [];
		this.cur = null;
		this.marquee = "Welcome to the Spectra Server Shop! - Shop overhaul by Lights";
	}

	footerHTML(marquee) {
		return `<div><div class="footerbuttons">${this.cur !== this.root ? '<button class="shopbutton" name="send" value="/shop root">Main Menu</button>' : ''}'${this.last && this.last.lenght && this.last.length > 0 ? ' <button class="shopbutton" name="send" value="/shop back">Back</button>' : ""} <button class="shopbutton" name="send" value="/shop exit">Exit</button></div><div class="broadcast-red"><marquee style="text-align: center; width: 100%" direction="left" speed="fast">${marquee}</marquee></div></div>`;
	}

	draw(marquee, item) {
		if (!this.root) return;
		if (typeof marquee === "undefined") marquee = this.marquee;
		const head = this.cur ? `|uhtmlchange|${this.user.userid}shop${this.user.shopCounter}|` : `|uhtml|${this.user.userid}shop${this.user.shopCounter}|`;
		if (!this.cur) this.cur = this.root;
		const html = `${Shop.headerHTML}${item ? this.cur[item] : this.cur.html}${this.footerHTML(marquee)}`;
		this.user.connections[0].sendTo(this.room.id, `${head}${html}`);
	}

	select(id) {
		let selected = false, item = null, marquee = this.cur.info ? this.cur.info : this.marquee;
		if (id === "root") {
			selected = this.root;
		} else if (this.cur.children.has(id)) {
			selected = this.cur.children.get(id);
			if (selected.type === "item") {
				marquee = selected.info;
				selected = this.cur;
				item = id;
			}
		}

		if (selected) {
			if (this.last.length > 15 && selected !== this.cur) this.last.shift();
			if (selected !== this.cur) {
				this.last.push(this.cur);
				this.cur = selected;
			}
			this.draw(marquee, item);
		}
	}

	back() {
		if (this.last.length === 0) return;
		this.cur = this.last[-1];
		this.last.splice(-1, 1);
		this.draw(this.cur.info ? this.cur.info : null, this.cur.type === "item" ? this.cur.id : null);
	}

	getItem(id) {
		if (!this.cur || !this.cur.children.has(id)) return null;
		const item = this.cur.children.get(id);
		return item.type === "item" ? item : null;
	}

	postPurchase(item, receipt) {
		this.user.connections[0].sendTo(this.room.id, `|uhtmlchange|${this.user.userid}shop${this.user.shopCounter}|${Shop.headerHTML}<center><b>Your purchase was successful</b></center><br><br><div style="width: 70%; margin: 0 auto;"><b>Name: </b>${this.user.name}<br><b>Date: </b>${receipt[1]}<br><b>Item: </b>${receipt[2]}<br><b>Price: </b>${receipt[3]} ${receipt[4]}<br><b>Remaining ${receipt[4]}: </b>${receipt[6]}<br><b>ReceiptID: </b>${receipt[5]}<br><br><center>This receipt is stored as your proof of purchase in the event of an error.<br>Use /receipts to see your recent receipts.<br><br><button class="shopbutton" name="send" value="/shop root">Main Menu</button> ${item.multibuy && receipt[3] < receipt[4] ? '<button class="shopbutton" name="send" value="/redeem id">Buy Another</button>' : ''}<br/></center></div>${this.footerHTML("Purchase Successful!")}`);
	}

	exit() {
		this.last = [];
		this.cur = null;
		this.root = null;
	}
}
