"use strict";

module.exports = class Economy {
	constructor() {
		this.currencies = new Map([["points", ["point", "Points"]]]);
	}

	init() {
		Spectra.UserClass.prototype.currency = new Map();
		Spectra.UserClass.initFunctions.push(function (callback) {
			const tasks = [];
			for (const [id] of Eco.currencies) {
				tasks.push(new Promise((resolve, reject) => {
					Eco.readCur(this.userid, id, value => {
						this.points = value;
						resolve();
					});
				}));
			}

			Promise.all(tasks).then(() => {return callback()});
		});

		Spectra.UserClass.prototype.getCur = function (obj) {
			const {callback} = obj;
			try {
				return callback(this.points || 0) || this.points || 0;
			} catch (e) {
				crash("Invalid Arguments", e);
				return callback(0) || 0;
			}
		};

		Spectra.UserClass.getCur = function (obj) {
			const {userid, callback} = obj;
			try {
				Spectra.checkUser(userid, err => {
					if (err) crash(...err);

					Eco.readCur(userid, "points", value => {
						return callback(value || 0);
					});
				});
			} catch (e) {
				crash("Invalid Arguments", e);
				return callback(0) || 0;
			}
		};

		Spectra.UserClass.prototype.addCur = function (obj) {
			const {val} = obj;
			this.points = (this.points || 0) + val;
			Spectra.UserClass.addCur({userid: this.userid, val: val});
		};

		Spectra.UserClass.addCur = function (obj) {
			const {userid, val} = obj;
			try {
				Spectra.checkUser(userid, err => {
					if (err) crash(...err);
					Eco.writeCur(userid, "points", val);
				});
			} catch (e) {
				crash("Invalid Arguments", e);
			}
		};

		return this;
	}

	readCur(userid, type, callback) {
		database.all("SELECT " + type + " FROM users WHERE userid=$userid", {
			$userid: userid,
		}, (err, rows) => {
			if (err) {
				crash("ReadCur Error", err);
				return callback(0);
			} else {
				return callback(rows[0] && rows[0][type] ? rows[0][type] : 0);
			}
		});
	}

	writeCur(userid, type, value) {
		Spectra.checkUser(userid, err => {
			if (err) crash(...err);

			database.run("UPDATE users SET " + type + "=" + type + " + $value WHERE userid=$userid", {
				$value: value,
				$userid: userid,
			}, err => {
				if (err) crash("WriteCur Error", err);
			});
		});
	}

	logTransaction(message) {
		//Dummy function for now
		if (!message) return false;
		//require("fs").appendFile('logs/point.log', '[' + new Date().toUTCString() + '] ' + message + '\n');
	}
};
