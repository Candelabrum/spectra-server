"use strict";

module.exports = class {
	constructor() {
		this.xpmod = 1;
		this.xpmodReason = null;
		this.formats = null;

		this.levels = [
			100,
			190,
			290,
			480,
			770,
			1100,
			1500,
			2070,
			2730,
			3550,
			4460,
			5550,
			6740,
			8120,
			9630,
			11370,
			13290,
			15520,
			18050,
			21060,
			24560,
			28310,
			32720,
			37290,
			42300,
			47440,
			52690,
			58100,
			63600,
			69250,
			75070,
			81170,
			87470,
			93970,
			100810,
			107980,
			115270,
			122960,
			131080,
			140080,
		];
	}

	init() {
		const tasks = [];

		tasks.push(new Promise((resolve, reject) => {
			Spectra.loadJSON('./spectra-data/tierpayout.json', (err, data) => {
				if (err) {
					crash("Tierpayout load Error", err, true);
				} else {
					this.formats = data;
					resolve();
				}
			});
		}));

		tasks.push(new Promise((resolve, reject) => {
			Spectra.loadJSON('./spectra-data/xpmods.json', (err, data) => {
				if (err) {
					crash("XP-Init Error", err, true);
				} else {
					this.usermods = data;
					this.cleanUsermods();
					resolve();
				}
			});
		}));

		Promise.all(tasks).then(() => {
			Spectra.UserClass.prototype.xp = 0;
			Spectra.UserClass.prototype.xpmod = 1;
			Spectra.UserClass.prototype.level = 1;

			Spectra.UserClass.initFunctions.push(function (callback) {
				XP.readXp(this.userid, value => {
					this.xp = value;
					this.level = XP.getLevel(value);
					this.xpmod = XP.getUserMod(this.userid);
					callback();
				});
			});

			Spectra.UserClass.prototype.getXp = function(callback) {
				Spectra.UserClass.getXp(xp => {
					return callback(xp);
				}, this.userid);
			};

			Spectra.UserClass.getXp = function(callback, userid) {
				XP.readXp(userid, value => {
					return callback(value);
				});
			}

			Spectra.UserClass.prototype.addXp = function (obj) {
				const {value, room} = obj;

				const level = this.level;
				let mod;

				switch (true) {
				case level < 5: mod = 1; break;
				case level >= 5 && level < 10: mod = 1.6; break;
				case level >= 10 && level < 15: mod = 2.2; break;
				case level >= 15 && level < 20: mod = 2.9; break;
				case level >= 20 && level < 25: mod = 3.7; break;
				case level >= 25 && level < 30: mod = 4.6; break;
				case level >= 30 && level < 35: mod = 5.6; break;
				case level >= 35 && level < 40: mod = 6.7; break;
				default: mod = 0; break;
				}

				let base = value + Math.floor(
					(
						Math.floor(
							Math.random() * 15
						) * (
							Math.round(
								Math.random()
							) === 1 ? -1 : 1
						)
					) * 0.1
				);

				//User XP Mod
				base = Math.floor(base * this.xpmod);

				//Global XP Mod
				base = Math.floor(base * XP.xpmod);

				//Apply Levelmod
				const final = Math.floor(base * mod) >= 0 ? Math.floor(base * mod) : 0;

				const oldXp = this.xp;
				this.xp += final;

				XP.update(this, oldXp, final, room);
			};

			Spectra.UserClass.addXp = function (obj) {
				const {id, value} = obj;
				Spectra.checkUser(id, err => {
					if (err) crash(...err);

					XP.readXp(id, xp => {
						//const oldXp = xp;
						const level = XP.getLevel(xp);
						let mod;

						switch (true) {
						case level < 5: mod = 1; break;
						case level >= 5 && level < 10: mod = 1.6; break;
						case level >= 10 && level < 15: mod = 2.2; break;
						case level >= 15 && level < 20: mod = 2.9; break;
						case level >= 20 && level < 25: mod = 3.7; break;
						case level >= 25 && level < 30: mod = 4.6; break;
						case level >= 30 && level < 35: mod = 5.6; break;
						case level >= 35 && level < 40: mod = 6.7; break;
						default: mod = 0; break;
						}

						let base = value + Math.floor(
							(
								Math.floor(
									Math.random() * 15
								) * (
									Math.round(
										Math.random()
									) === 1 ? -1 : 1
								)
							) * 0.1
						);

						XP.getUserMod(this, (err, xpmod) => {
							if (err) crash("XPUser GetuserMod Error", err);

							//User XP Mod
							base = Math.floor(base * (!err && xpmod ? xpmod : 1));

							//Global XP Mod
							base = Math.floor(base * XP.xpmod);

							//Apply Levelmod
							const final = Math.floor(base * mod) >= 0 ? Math.floor(base * mod) : 0;
							XP.write(id, final);
						});
					});
				});
			};
		});

		return this;
	}

	update(user, oldXp, added, room) {
		this.writeXp(user.userid, added);

		if (user.connections[0] !== 'undefined') {
			//Add Notification Message
			user.connections[0].sendTo(
				room.id,
				`You've gained ${added} XP${this.xpmod !== 1 ? ` - [Serverwide Modifier: ${this.xpmod}X${this.xpmodReason ? ` - Reason: ${this.xpmodReason}]` : "]"}` : ""}${user.xpmod !== 1 ? ` - [${user.xpmod}X]` : ""}`
			);
			if (room.update) room.update();

			//Check for Levelup
			let level = this.getLevel(user.xp);
			if (level > user.level) {
				user.level = level;

				//Add Rewards in Later
				let reward = this.rewardTier(level);
				if (!reward) {
					reward = "15 Points.";
					user.addCur({userid: user.userid, type: "points", val: 15});
				}
				user.connections[0].sendTo(
					room.id,
					`|html|<center><font size=4><b><i>Level Up!</i></b></font><br>You Have reached level ${level}, this will award you:<br><b>${reward}</b></center>`
				);
			}
		}
	}

	rewardTier(level) {
		switch(level) {
		case 3: return "The ability to write an 'About Me' on your Profile.";//Profile AboutMe
		case 7: return "The ability to change the background color of your Profile";//Profile color
		case 10: return "The ability to freely change your avatar.";//Custom Avatar
		case 13: return "The ability to customize the team of 6 pokemon on your Profile.";//Profile Pokemon
		case 15: return "The ability to freely change your name color.";//Custom Color
		case 18: return "The ability to change the secondary colors on your Profile.";//Profile Secondary Color
		case 20: return "The ability to freely change your userlist Icon.";//Custom Icon
		case 25: return "A special title specific to you.";//Title
		case 30: return "The ability to freely change your rank symbol.";//Custom Symbol
		case 35: return "The ability to change the color of your text in chat.";//Custom Chat Text Color
		case 40: return "A special CSS display for your PMs.";//Custom PM CSS
		default: return null;
		}
	}

	cleanUsermods() {
		let entry;
		for (const i in this.usermods) {
			entry = this.usermods[i];

			if (entry.expire < Date.now()) {
				//Remove xpmod from user object
				Users(i).xpmod = 1;

				//remove entry from xpmod obj
				delete this.usermods[i];
			}
		}

		this.writeJSON();
	}

	getUserMod(userid) {
		return this.usermods[userid] || 1;
	}

	getPercentage(xp) {
		const level = this.getLevel(xp);
		const index = level - 2;
		const curLevelXP = index < 0 ? 0 : this.levels[index];
		const nextLevelXP = this.levels[index + 1];
		return [
			nextLevelXP - xp, //Remaining
			(((xp + curLevelXP) / (nextLevelXP - curLevelXP)) * 100).toFixed(0)
		];
	}

	writeJSON() {
		Spectra.writeJSON('./spectra-data/xpmods.json', JSON.stringify(this.usermods), err => {
			if (err) crash("XP-writeJSON Error", err);
		});
	}

	writeXp(userid, value) {
		database.run("UPDATE users SET XP = XP + $xp WHERE userid = $userid", {
			$xp: value,
			$userid: userid,
		}, err => {
			if (err) crash("Write-XP Error", err);
		});
	}

	readXp(userid, callback) {
		new Promise((resolve, reject) => {
			Spectra.checkUser(userid, err => {
				if (err) {
					reject(err);
				} else {
					resolve();
				}
			});
		}).then(() => {
			database.all("SELECT XP FROM users WHERE userid=$userid", {
				$userid: userid,
			}, (err, results) => {
				if (err) {
					crash("Read-XP Error", err);
					return callback(0);
				} else {
					return callback((typeof results[0] === "undefined") ? 0 : results[0].XP);
				}
			});
		}).catch(err => crash(err[0], err[1]));
	}

	getLevel(xp) {
		for (let level = 1; level < this.levels.length; level++) {
			if (this.levels[level] >= xp) return level;
		}
	}
};
