"use strict";

const msgpack = require("msgpack-js");

const colorArr = [
	"50d35b", //Common
	"509cd3", //Uncommon
	"9850d3", //Rare
	"d36a50" //Everything Else
];

module.exports = class {
	constructor() {
		this.sets = new Map();
		this.users = {};
		this.ready = false;
		this.rarityMap = new Map([
			["Common", "#cecece"],
			["Uncommon", "#7ad160"],
			["Rare", "#6094d1"],
			["Rare Holo", "#0275f9"],
			["Rare Holo EX", "#843fd3"],
			["Rare Holo GX", "#d33fc4"],
			["Rare Holo Lv.X", "#ef283f"],
			["Rare Prime", "#c10f24"],
			["LEGEND", "#ddb225"],
			["Rare Ultra", "#9300ef"],
			["Rare ACE", "#e0d021"],
			["Rare BREAK", "#21cae0"],
			["Rare Rainbow", "#e02121|#e0aa21|#e0e021|#21e024|#21aae0|#2821e0|#c321e0"],
			["Promo", "#21e03b"]
		]);
	}

	init() {
		Spectra.loadMsgpacks("cards", (id, data) => {
			//This callback is fired for each file
			let obj;
			try {
				obj = msgpack.decode(data);
			} catch (e) {
				crash("Error loading Msgpack card file " + id, e);
				obj = null;
			}
			if (obj) {
				//Data was successfully loaded, parse it
				//We can fix this up later to get rid of some of the arbitrary structure
				obj = obj.sets;
				let setid, setdata, carddata, cardsetobject = {};
				for (const set of obj) {

					//remnant from jsonpickle py library
					if (set.data["py/object"]) delete set.data["py/object"];

					this.sets.set(set.id, set.data);
				}
			}
		});

		//Wait for card API to be ready before we allow commands.
		Spectra.on("cards", () => {
			this.ready = true;
		});

		return this;
	}

	getSet(setId) {
		let set = this.sets.get(setId);
		if (!set) {
			//Invalid set, try to find it
			set = null;
			for (const [id, data] of this.sets) {
				if (toId(setId.includes(toId(id))) || toId(setId.includes(toId(data.name)))) {
					set = id;
					setId = id;
					break;
				}
			}
			if (!set) return null; //Cant find set
			set = this.sets.get(setId);
			if (!set) return null;
		}
	}

	getCard(str) {
		if (!str) return null;
		str = str.trim(); //Clear leading and training spaces

		let delim;
		switch(true) {
		case str.includes("-"): delim = "-"; break;
		case str.includes(","): delim = ","; break;
		default: delim = " ";
		}

		let parts = str.split(delim);

		if (parts.length !== 2) {
			//We couldn't find a delim character, try to find a valid set id
			let failed = parts[0];
			parts = [];
			for (const [id, data] of this.sets) {
				if (toId(failed.includes(toId(id)))) {
					parts.push(toId(id)); //Add set ID to array
					parts.push(toId(failed).replace(toId(id), ""));
					break;
				}

				if (toId(failed.includes(toId(data.name)))) {
					parts.push(toId(id)); //Add set ID to array
					parts.push(toId(failed).replace(toId(data.name), ""));
					break;
				}
			}

			if (parts.length === 0 || parts[1] === "") return null; //Cant find a set
		} else {
			parts = parts.map(x => toId(x));
		}

		//Now we have an array of toId'd set and identifier
		//Lets see if we can find a card or if we need to resolve further
		let [setId, cardId] = parts;
		let set = this.getSet(setId);
		if (!set) return null;

		//Ok, we found a set. great. Lets find the card
		let card = null;

		//Define Error Tolerance range
		const RANGE = [2, 4, 6, 8];
		let deviation = 0;
		for (deviation; deviation < RANGE.length; deviation++) {
			if (cardId.length <= RANGE[deviation]) break;
		}
		const MAX_DEVIATION = deviation;

		//Search
		for (const i of set.cards) {
			if (toId(i.id) === toId(`${setId}${cardId}`) || Dex.levenshtein(toId(cardId),toId(i.name),MAX_DEVIATION) <= MAX_DEVIATION || toId(i.number) == toId(cardId)) {
				card = i;
				break;
			}
		}

		return card;
	}

	rarityDisplay(rarity) {
		if (!rarity) rarity = "Promo";
		let color = this.rarityMap.get(rarity);
		let html = "";
		if (rarity === "Rare Rainbow") {
			//I hate my life
			color = color.split("|");
			html = `<font color="${color[5]}">Rare </font>`;
			const letters = ["R","a","i","n","b","o","w"];
			for (let i = 0; i < 7; i++) {
				html += `<font color="${color[i]}">${letters[i]}</font>`;
			}
		} else {
			html = `<font color="${color}">${rarity}</font>`;
		}
		return html;
	}
}